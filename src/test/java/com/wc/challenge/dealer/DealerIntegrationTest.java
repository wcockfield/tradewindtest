package com.wc.challenge.dealer;

import com.wc.challenge.model.RPSGame;
import com.wc.challenge.model.PlayerInfo;
import com.wc.challenge.strategy.RPSStrategy;
import com.wc.challenge.util.WebClientUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "file:./casino-integrationtest.properties")
@ComponentScan(basePackages = {"com.wc.challenge"}, excludeFilters={
        @ComponentScan.Filter(type= FilterType.REGEX, pattern={"com.wc.challenge.dealer.*",
                "com.wc.challenge.player.*","com.wc.challenge.account.*","com.wc.challenge.game.service.*"})
})
public class DealerIntegrationTest {

    private int playerServicePort = 9000;

    @Autowired
    private WebClientUtil webClientUtil;

    private WebClient webClient;

    @Test
    public void getListOfPendingGames() {

        ClientResponse clientResponse = webClientUtil.getData(webClientUtil.getWebclient(8080),"/player/reg-bal/100");

        UUID uuid = clientResponse.toEntity(UUID.class).block().getBody();
        assertTrue(webClientUtil.validUUID(uuid.toString()));

        PlayerInfo pi = new PlayerInfo(uuid, new RPSStrategy());
        webClientUtil.postData(webClient, pi,"/dealer/new-game/player");

        clientResponse = webClientUtil.getData(webClient,"/dealer/list-pending-games");

        List<RPSGame> gamesList = clientResponse.toEntity(List.class).block().getBody();

        assertNotNull(gamesList);
        assertTrue(gamesList.size() >= 1);
    }

    @Test
    public void getPlayerBalance() {

        ClientResponse clientResponse = webClientUtil.getData(webClientUtil.getWebclient(8080),"/player/reg-bal/100");

        UUID uuid = clientResponse.toEntity(UUID.class).block().getBody();
        assertTrue(webClientUtil.validUUID(uuid.toString()));

        PlayerInfo pi = new PlayerInfo(uuid, new RPSStrategy());
        webClientUtil.postData(webClient, pi,"/dealer/new-game/player");

        clientResponse = webClientUtil.getData(webClient,"/account/player-bal/"+pi.getPlayerInfoId());
        Integer balance = clientResponse.toEntity(Integer.class).block().getBody();

        assertTrue(balance == 95);
    }

    @Test
    public void getHouseBalance() {
        ClientResponse clientResponse = webClientUtil.getData(webClient,"/account/house-bal/");
        Integer balance = clientResponse.toEntity(Integer.class).block().getBody();
        assertTrue(balance>=0);
        System.out.println("Balance " +balance);
    }

    @PostConstruct
    public void setWebClient() {
        webClient = webClientUtil.getWebclient(playerServicePort);
    }
}