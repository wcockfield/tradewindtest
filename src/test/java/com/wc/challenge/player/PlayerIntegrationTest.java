package com.wc.challenge.player;

import com.wc.challenge.strategy.RPSStrategy;
import com.wc.challenge.util.WebClientUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.PostConstruct;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "file:./casino-integrationtest.properties")
@ComponentScan(basePackages = {"com.wc.challenge"}, excludeFilters={
        @ComponentScan.Filter(type= FilterType.REGEX, pattern={"com.wc.challenge.dealer.*.*",
                "com.wc.challenge.player.*","com.wc.challenge.account.*","com.wc.challenge.game.*"})
})
public class PlayerIntegrationTest {

    private int playerServicePort=8080;

    @Autowired
    private WebClientUtil webClientUtil;

    private WebClient webClient;

    @Test
    public void registerWithInitialBalanceTest() {

        ClientResponse clientResponse = registerPlayerBalance();

        UUID uuid = clientResponse.toEntity(UUID.class).block().getBody();

        assertNotNull(uuid);
        assertTrue(webClientUtil.validUUID(uuid.toString()));
    }

    @Test
    public void requestNewGameTest() {

        ClientResponse clientResponse = registerPlayerBalance();

        UUID uuid = clientResponse.toEntity(UUID.class).block().getBody();
        assertNotNull(uuid);
        assertTrue(webClientUtil.validUUID(uuid.toString()));

        ClientResponse clientResponse2 = webClientUtil.postData(webClient, new RPSStrategy(),
                "/player/new-game/"+uuid.toString() + "/strategy");
        UUID gameId = clientResponse2.toEntity(UUID.class).block().getBody();

        ClientResponse clientResponseWithGames = webClientUtil.getData(webClientUtil.getWebclient(9000),
                "/dealer/list-pending-games");
        List<LinkedHashMap<String,Object>> gamesList = clientResponseWithGames.toEntity(List.class).block().getBody();

        assertTrue(clientResponse2.statusCode().equals(HttpStatus.OK));
        assertTrue(containsGame(gamesList, gameId));
    }

    private boolean containsGame(List<LinkedHashMap<String,Object>> gamesList, UUID gameId) {
        for (LinkedHashMap<String,Object> gameVals: gamesList) {
            if ( gameVals.get("gameId").equals(gameId.toString()) ) {
                return true;
            }
        }
        return false;
    }

    @Test
    public void JoinGameTest() {

        // Register player 1
        ClientResponse clientResponse = registerPlayerBalance();

        UUID uuid = clientResponse.toEntity(UUID.class).block().getBody();
        assertNotNull(uuid);
        assertTrue(webClientUtil.validUUID(uuid.toString()));

        ClientResponse clientResponse2 = webClientUtil.postData(webClient, new RPSStrategy(),
                "/player/new-game/"+uuid.toString() + "/strategy");

        assertTrue(clientResponse2.statusCode().equals(HttpStatus.OK));


        // Register player 2
        ClientResponse clientResponse3 = registerPlayerBalance();

        UUID uuidP2 = clientResponse3.toEntity(UUID.class).block().getBody();
        assertNotNull(uuidP2);
        RPSStrategy strat = new RPSStrategy();
        //strat.setMove(RPSMove.SCISSORS);
        ClientResponse clientResponse4 = webClientUtil.postData(webClient, strat,
                "/player/join-game/"+uuidP2.toString() + "/strategy");

        assertTrue(clientResponse4.statusCode().equals(HttpStatus.OK));

    }

    private ClientResponse registerPlayerBalance() {
        return webClientUtil.getData(webClient,"/player/reg-bal/100");
    }

    @PostConstruct
    public void setWebClient() {
        webClient = webClientUtil.getWebclient(playerServicePort);
    }
}
