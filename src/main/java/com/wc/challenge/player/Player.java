package com.wc.challenge.player;

import com.wc.challenge.player.service.IPlayer;
import com.wc.challenge.strategy.RPSStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class Player {

    private static final Logger LOGGER = LogManager.getLogger(Player.class.getName());

    @Autowired
    private IPlayer playerService;

    @RequestMapping("/player/reg-bal/{balance}")
    public UUID registerWithInitialBalance(@PathVariable Integer balance) {

        LOGGER.info("Setting balance and strategy....");
        return playerService.registerWithInitialBalance(balance);
    }

    @RequestMapping("/player/new-game/{uuid}/strategy")
    public UUID playNewGame(@PathVariable UUID uuid, @RequestBody RPSStrategy strategy) {
        return playerService.playNewGame(uuid, strategy);
    }

    @RequestMapping("/player/join-game/{uuid}/strategy")
    public Boolean joinGame(@PathVariable UUID uuid, @RequestBody RPSStrategy strategy) {
        return playerService.joinGame(uuid, strategy);
    }

    @RequestMapping("/player/join-specific-game/{playerId}/{gameId}/strategy")
    public Boolean joinGame(@PathVariable UUID playerId, @PathVariable UUID gameId,
                            @RequestBody RPSStrategy strategy) {
        return playerService.joinSpecificGame(playerId, strategy, gameId);
    }
}
