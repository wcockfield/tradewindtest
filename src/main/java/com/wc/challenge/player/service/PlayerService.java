package com.wc.challenge.player.service;

import com.wc.challenge.error.OperationFailedException;
import com.wc.challenge.model.PlayerInfo;
import com.wc.challenge.strategy.RPSStrategy;
import com.wc.challenge.util.WebClientUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.PostConstruct;

import java.util.*;

@Service
public class PlayerService implements IPlayer {

    private WebClientUtil webClientUtil;
    private WebClient webClient;

    @Autowired
    public void setWebClientUtil(WebClientUtil webClientUtil) {
        this.webClientUtil = webClientUtil;
    }

    @Override
    public UUID registerWithInitialBalance(Integer balance) {
        PlayerInfo pi = new PlayerInfo(balance);
        webClientUtil.postData(webClient, pi, "/dealer/register-player/player");
        return pi.getPlayerInfoId();
    }

    @Override
    public UUID playNewGame(UUID playerUUID, RPSStrategy strategy) {
        PlayerInfo pi = new PlayerInfo(playerUUID, strategy);
        ClientResponse clientResponse = webClientUtil.postData(webClient, pi, "/dealer/new-game/player");
        webClientUtil.checkIfError(clientResponse);
        return clientResponse.toEntity(UUID.class).block().getBody();
    }

    @Override
    public Boolean joinGame(UUID playerUUID, RPSStrategy strategy) {

        PlayerInfo pi = new PlayerInfo(playerUUID, strategy);

        ClientResponse clientResponse = webClientUtil.getData(webClient,"/dealer/list-pending-games");
        webClientUtil.checkIfError(clientResponse);
        List<LinkedHashMap<String,Object>> gamesList = clientResponse.toEntity(List.class).block().getBody();

        // Pick the first available game.
        if ( !gamesList.isEmpty() ) {
            clientResponse = webClientUtil.postData(webClient, pi, "/dealer/join-game/" + gamesList.get(0).get("gameId") + "/player");
            webClientUtil.checkIfError(clientResponse);
            return true;
        }
        throw new OperationFailedException("There are no games to join.");
    }

    @Override
    public Boolean joinSpecificGame(UUID playerId, RPSStrategy strategy, UUID gameId) {

        PlayerInfo pi = new PlayerInfo(playerId, strategy);
        ClientResponse clientResponse = webClientUtil.postData(webClient, pi,
                "/dealer/join-game/"+ gameId +"/player");
        webClientUtil.checkIfError(clientResponse);
        return true;
    }

    @PostConstruct
    private void createWebClient() {
         webClient = webClientUtil.getWebclient(9000);
    }
}
