package com.wc.challenge.player.service;

import com.wc.challenge.strategy.RPSStrategy;

import java.util.UUID;

public interface IPlayer {
    UUID registerWithInitialBalance(Integer balance);
    UUID playNewGame(UUID playerUUID, RPSStrategy strategy);
    Boolean joinGame(UUID playerUUID, RPSStrategy strategy);
    Boolean joinSpecificGame(UUID playerId, RPSStrategy strategy, UUID gameId);
}