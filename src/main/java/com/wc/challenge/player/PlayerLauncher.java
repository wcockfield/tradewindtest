package com.wc.challenge.player;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

@SpringBootApplication
@ComponentScan(basePackages = {"com.wc.challenge"}, excludeFilters={
        @ComponentScan.Filter(type=FilterType.REGEX, pattern={"com.wc.challenge.account.*",
                "com.wc.challenge.dealer.*","com.wc.challenge.game.*"})
})
public class PlayerLauncher {

    public static void main(String[] args) {
        SpringApplication.run(PlayerLauncher.class, args);
    }
}

