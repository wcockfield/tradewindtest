package com.wc.challenge.strategy;

import com.wc.challenge.enums.RPSMove;

import java.util.UUID;

public class RPSStrategy {

    private RPSMove move = RPSMove.ROCK;

    public RPSMove getMove() {
        return move;
    }

    public void setMove(RPSMove move) {
        this.move = move;
    }

    public RPSMove playGame(UUID gameId) {
        // A sample dumb strategy that ignores the gameId.
        // Our engineers have implemented some sophisticated
        // strategies and are waiting for you to build the Casino.
        return move;
    }
}
