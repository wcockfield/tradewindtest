package com.wc.challenge.util;

import com.wc.challenge.error.OperationFailedException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class WebClientUtil {

    private ExchangeFilterFunction logRequest() {
        return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
            // Need to add logger
            //log.info("Request: {} {}", clientRequest.method(), clientRequest.url());
            System.out.println("Request: " + clientRequest.method() + " : " + clientRequest.url());
            clientRequest.headers().forEach((name, values) -> values.forEach(value -> System.out.println("Name , Value " +  name + " : " + value)));
            return Mono.just(clientRequest);
        });
    }

    public WebClient getWebclient(Integer port) {

        return WebClient.builder()
                .baseUrl("http://localhost:"+port)
                .filter(logRequest())
                .build();
    }

    public boolean validUUID(String s){
        if (s != null) {
            return s.matches("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$");
        }
        return false;
    }

    public ClientResponse postData(WebClient webClient, Object objData, String path) {

        return webClient.post()
                .uri(uriBuilder -> uriBuilder.path(path).build())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .body(BodyInserters.fromObject(objData))
                .exchange()
                .block();
    }

    public ClientResponse getData(WebClient webClient,String path) {

        return webClient.get()
                .uri(uriBuilder -> uriBuilder.path(path).build())
                .exchange()
                .block();
    }

    public void checkIfError(ClientResponse clientResponse) {
        if ( clientResponse.statusCode().isError()) {
            throw new OperationFailedException(clientResponse.statusCode().getReasonPhrase());
        }
    }
}
