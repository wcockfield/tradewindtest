package com.wc.challenge.event;

import com.wc.challenge.enums.EventType;

import java.time.Instant;
import java.util.UUID;

public class EventInfo {

    private EventType eventType;
    private UUID playerId;
    private UUID eventId;
    private long timestampInMS;
    private UUID gameId;

    public EventInfo(EventType eventType) {
        eventInfo(eventType, null, null);
    }

    public EventInfo(EventType eventType, UUID playerId, UUID gameId) {
        eventInfo(eventType, playerId, gameId);
    }

    private void eventInfo(EventType eventType, UUID playerId, UUID gameId) {
        this.eventType = eventType;
        this.playerId = playerId;
        eventId = UUID.randomUUID();
        this.timestampInMS = Instant.now().toEpochMilli();
        this.gameId = gameId;
    }

    public long getTimestampInMS() {
        return timestampInMS;
    }

    // A unique id for this event.
    public java.util.UUID getEventID() {
        return eventId;
    }

    public EventType getEventType() {
        return eventType;
    }

    // Only meaningful for the JOIN_GAME event type. Otherwise returns null.
    // Indicates the game which the player want's to join by pointing to the event that
    // resulted in that game being established.
    public java.util.UUID getEstablishNewGameEventId() {
        return gameId;
    }

    // Only meaningful for the JOIN_GAME event type. Otherwise returns null.
    public java.util.UUID getPlayerInfoID() {
        return playerId;
    }
}
