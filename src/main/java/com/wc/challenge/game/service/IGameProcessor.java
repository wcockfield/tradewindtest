package com.wc.challenge.game.service;

import com.wc.challenge.model.RPSGame;

public interface IGameProcessor {
    void processGames(RPSGame rpsGame);
}
