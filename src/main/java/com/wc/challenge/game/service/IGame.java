package com.wc.challenge.game.service;

import com.wc.challenge.model.RPSGame;
import com.wc.challenge.model.PlayerInfo;

import java.util.Collection;
import java.util.UUID;

public interface IGame {

    void queueGame(RPSGame rpsGame);
    void joinGame(UUID gameId, PlayerInfo playerInfo);
    Collection<RPSGame> getPendingGames();
}
