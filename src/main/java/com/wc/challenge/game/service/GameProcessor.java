package com.wc.challenge.game.service;

import com.wc.challenge.constants.RPSGameConst;
import com.wc.challenge.enums.RPSMove;
import com.wc.challenge.enums.RPSWinner;
import com.wc.challenge.model.RPSGame;
import com.wc.challenge.account.service.IAccount;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;


@Service
public class GameProcessor implements IGameProcessor {

    private static final Logger LOGGER = LogManager.getLogger(GameProcessor.class.getName());

    private IAccount accountService;

    @Autowired
    public void setAccountService(IAccount account) {
        this.accountService = account;
    }

    @JmsListener(destination = "game-queue", containerFactory = "myFactory")
    public void processGames(RPSGame rpsGame) {
        playGame(rpsGame);
    }

    private void playGame(RPSGame rpsGame) {

        LOGGER.info("Play game and determine the winner.");
        RPSWinner winner = determineWinner(rpsGame.getPlayer1().getRpsStrategy().playGame(rpsGame.getGameId()),
                rpsGame.getPlayer2().getRpsStrategy().playGame(rpsGame.getGameId()));

        payWinnner(rpsGame, winner);
    }

    private void payWinnner(RPSGame rpsGame, RPSWinner winner) {
        LOGGER.info("Pay game winner.");
        if (winner == RPSWinner.PLAYER_ONE) {
            accountService.credit(rpsGame.getPlayer1(), RPSGameConst.GAME_PAYOUT);
        } else if (winner == RPSWinner.PLAYER_TWO) {
            accountService.credit(rpsGame.getPlayer2(), RPSGameConst.GAME_PAYOUT);
        }
    }

    private RPSWinner determineWinner(RPSMove p1Move, RPSMove p2Move) {
        if ( p1Move == p2Move ) {
            return RPSWinner.DRAW;
        }

        if ( p1Move == RPSMove.ROCK && p2Move == RPSMove.SCISSORS ||
                p1Move == RPSMove.PAPER && p2Move == RPSMove.ROCK ||
                p1Move == RPSMove.SCISSORS && p2Move == RPSMove.PAPER) {
            return RPSWinner.PLAYER_ONE;
        }

        return RPSWinner.PLAYER_TWO;
    }
}
