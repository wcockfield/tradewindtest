package com.wc.challenge.game.service;

import com.wc.challenge.constants.ServletContextConst;
import com.wc.challenge.error.OperationFailedException;
import com.wc.challenge.model.RPSGame;
import com.wc.challenge.model.PlayerInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import java.util.*;

@Service
public class GameService implements IGame {

    private JmsTemplate jmsTemplate;

    private ServletContext servletContext;

    @Autowired
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Autowired
    public void setJmsTemplate(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void queueGame(RPSGame rpsGame) {
        getPendingGamesMap().put(rpsGame.getGameId(), rpsGame);
    }

    @Override
    public void joinGame(UUID gameId, PlayerInfo playerInfo) {

        RPSGame rpsGame = getPendingGamesMap().remove(gameId);
        if ( rpsGame == null ) {
            throw new OperationFailedException("The game with Id(" + gameId + ") is no longer available.");
        }
        if ( rpsGame.getPlayer1().getPlayerInfoId().equals(playerInfo.getPlayerInfoId()) ) {
            getPendingGamesMap().put(gameId, rpsGame);
            throw new OperationFailedException("You can not play against yourself :-).");
        }
        rpsGame.setPlayer2(playerInfo);

        jmsTemplate.convertAndSend("game-queue", rpsGame);
    }

    @Override
    public Collection<RPSGame> getPendingGames() {
        return getPendingGamesMap().values();
    }

    private Map<UUID, RPSGame> getPendingGamesMap() {
        return ((Map<UUID, RPSGame>)servletContext.getAttribute(ServletContextConst.PENDING_GAMES));
    }
}
