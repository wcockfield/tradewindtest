package com.wc.challenge.model;

import com.wc.challenge.strategy.RPSStrategy;

import java.util.UUID;

public class PlayerInfo {

    private int startingAccountBalance;
    private RPSStrategy rpsStrategy;
    private UUID playerInfoId;

    public PlayerInfo() { }

    public PlayerInfo(int initBalance) {
        this.startingAccountBalance = initBalance;
        this.rpsStrategy = null;
        this.playerInfoId = UUID.randomUUID();
    }

    public PlayerInfo(UUID playerUUID, RPSStrategy strategy) {
        this.startingAccountBalance = 0;
        this.rpsStrategy = strategy;
        this.playerInfoId = playerUUID;
    }

    public int getStartingAccountBalance() {
        return startingAccountBalance;
    }

    public void setStartingAccountBalance(int startingAccountBalance) {
        this.startingAccountBalance = startingAccountBalance;
    }

    public RPSStrategy getRpsStrategy() {
        return rpsStrategy;
    }

    public void setRpsStrategy(RPSStrategy rpsStrategy) {
        this.rpsStrategy = rpsStrategy;
    }

    public UUID getPlayerInfoId() {
        return playerInfoId;
    }

    public void setPlayerInfoId(UUID playerInfoId) {
        this.playerInfoId = playerInfoId;
    }
}
