package com.wc.challenge.model;

import com.wc.challenge.model.PlayerInfo;


import java.util.UUID;

public class RPSGame {

    private UUID gameId;
    private PlayerInfo player1;
    private PlayerInfo player2;

    public RPSGame() {
        gameId = UUID.randomUUID();
    }

    public java.util.UUID getGameId() {
        return gameId;
    }

    public PlayerInfo getPlayer1() {
        return player1;
    }

    public void setPlayer1(PlayerInfo player1) {
        this.player1 = player1;
    }

    public PlayerInfo getPlayer2() {
        return player2;
    }

    public void setPlayer2(PlayerInfo player2) {
        this.player2 = player2;
    }
}
