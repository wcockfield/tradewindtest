package com.wc.challenge.dealer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

@SpringBootApplication
@ComponentScan(basePackages = {"com.wc.challenge"}, excludeFilters={
        @ComponentScan.Filter(type=FilterType.REGEX, pattern={"com.wc.challenge.player.*"})
})
public class DealerLauncher {

    public static void main(String[] args) {
        SpringApplication.run(DealerLauncher.class, args);
    }
}