package com.wc.challenge.dealer.config;

import com.wc.challenge.constants.ServletContextConst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import javax.servlet.ServletContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Configuration
@EnableAsync
public class DealerConfig {

    private ServletContext servletContext;

    @Autowired
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;

        servletContext.setAttribute(ServletContextConst.PLAYER_ACCOUNTS,
                Collections.synchronizedMap(new HashMap<>()));
        servletContext.setAttribute(ServletContextConst.PENDING_GAMES,Collections.synchronizedMap(new HashMap<>()));
        servletContext.setAttribute(ServletContextConst.HOUSE_ACCOUNT,new AtomicInteger(0));
    }
}