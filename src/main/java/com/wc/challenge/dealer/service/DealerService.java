package com.wc.challenge.dealer.service;

import com.wc.challenge.constants.RPSGameConst;
import com.wc.challenge.error.OperationFailedException;
import com.wc.challenge.model.RPSGame;
import com.wc.challenge.model.PlayerInfo;
import com.wc.challenge.account.service.IAccount;
import com.wc.challenge.game.service.IGame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.UUID;

@Service
public class DealerService implements IDealer {

    private IAccount accountService;
    private IGame gameService;

    @Autowired
    public void setAccountService(IAccount accountService) {
        this.accountService = accountService;
    }

    @Autowired void setGameService(IGame gameService) { this.gameService = gameService; }

    @Override
    public void registerWithInitialBalance(PlayerInfo player) {
        accountService.registerBalance(player);
    }

    @Override
    public UUID startNewGame(PlayerInfo player) {

        if (!accountService.hasEnoughFunds(player.getPlayerInfoId(),  RPSGameConst.GAME_COST)) {
            throw new OperationFailedException("Insufficient funds to play or you are not registered.");
        }
        accountService.debit(player, RPSGameConst.GAME_COST);
        RPSGame rpsGame = new RPSGame();
        rpsGame.setPlayer1(player);
        gameService.queueGame(rpsGame);

        return rpsGame.getGameId();
    }

    @Override
    public void joinGame(UUID gameId, PlayerInfo player) {

        if (!accountService.hasEnoughFunds(player.getPlayerInfoId(), RPSGameConst.GAME_COST)) {
            throw new OperationFailedException("Insufficient funds to play or you are not registered.");
        }

        try {
            accountService.debit(player, RPSGameConst.GAME_COST);
            gameService.joinGame(gameId, player);
        } catch (OperationFailedException ofe) {
            accountService.credit(player, RPSGameConst.GAME_COST);
            throw ofe;
        }
    }

    @Override
    public Collection<RPSGame> getPendingGames() {
        return gameService.getPendingGames();
    }
}
