package com.wc.challenge.dealer.service;

import com.wc.challenge.model.RPSGame;
import com.wc.challenge.model.PlayerInfo;

import java.util.Collection;
import java.util.UUID;

public interface IDealer {

    void registerWithInitialBalance(PlayerInfo player);
    UUID startNewGame(PlayerInfo player);
    void joinGame(UUID gameId, PlayerInfo player);
    Collection<RPSGame> getPendingGames();
}
