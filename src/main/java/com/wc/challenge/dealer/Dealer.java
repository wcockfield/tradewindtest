package com.wc.challenge.dealer;

import com.wc.challenge.model.RPSGame;
import com.wc.challenge.model.PlayerInfo;
import com.wc.challenge.dealer.service.IDealer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.UUID;

@RestController
public class Dealer {

    private static final Logger LOGGER = LogManager.getLogger(Dealer.class.getName());

    private IDealer dealerService;

    @Autowired
    public void setDealerService(IDealer dealerService) { this.dealerService = dealerService; }

    @RequestMapping("/dealer/register-player/player")
    public void registerWithInitialBalance(@RequestBody PlayerInfo player) {

        LOGGER.info("Registering player");
        dealerService.registerWithInitialBalance(player);
    }

    @RequestMapping("/dealer/new-game/player")
    public UUID startNewGame(@RequestBody PlayerInfo playerInfo) {
        return dealerService.startNewGame(playerInfo);
    }

    @RequestMapping("/dealer/join-game/{gameId}/player")
    public void joinGame(@PathVariable UUID gameId, @RequestBody PlayerInfo player)  {
        dealerService.joinGame(gameId, player);
    }

    @RequestMapping("/dealer/list-pending-games")
    public Collection<RPSGame> getPendingGames() {
        return dealerService.getPendingGames();
    }
}
