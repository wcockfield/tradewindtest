package com.wc.challenge.account;


import com.wc.challenge.account.service.IAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class Account {

    private IAccount accountService;

    @Autowired
    public void setAccountService(IAccount accountService) {
        this.accountService = accountService;
    }

    @RequestMapping("/account/house-bal")
    public Integer houseBalance() {
        return accountService.houseBalance();
    }

    @RequestMapping("/account/player-bal/{playerId}")
    public Integer playerBalance(@PathVariable UUID playerId) {
        return accountService.playerBalance(playerId);
    }
}
