package com.wc.challenge.account.service;

import com.wc.challenge.model.PlayerInfo;

import java.util.UUID;

public interface IAccount {

    Boolean hasEnoughFunds(UUID playerId, Integer amount);
    void registerBalance(PlayerInfo player);
    void credit(PlayerInfo playerInfo, Integer amount);
    void debit(PlayerInfo playerInfo, Integer amount);
    Integer houseBalance();
    Integer playerBalance(UUID playerId);
}
