package com.wc.challenge.account.service;

import com.wc.challenge.constants.ServletContextConst;
import com.wc.challenge.model.PlayerInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class AccountService implements IAccount {

    private ServletContext servletContext;

    @Autowired
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Override
    public void registerBalance(PlayerInfo player) {
        getPlayerAccounts().put(player.getPlayerInfoId(), new AtomicInteger(player.getStartingAccountBalance()));
    }

    public Boolean hasEnoughFunds(UUID playerId, Integer amount) {
        return getPlayerAccounts().get(playerId).get() - amount >= 0;
    }

    @Override
    public void credit(PlayerInfo playerInfo, Integer amount) {
        getPlayerAccounts().get(playerInfo.getPlayerInfoId()).addAndGet(amount);
        getHouseAccount().addAndGet(amount*-1);
    }

    @Override
    public void debit(PlayerInfo playerInfo, Integer amount) {
        getPlayerAccounts().get(playerInfo.getPlayerInfoId()).addAndGet(amount*-1);
        getHouseAccount().addAndGet(amount);
    }

    @Override
    public Integer houseBalance() {
        return getHouseAccount().get();
    }

    @Override
    public Integer playerBalance(UUID playerId) {
        return getPlayerAccounts().get(playerId).get();
    }

    private Map<UUID, AtomicInteger> getPlayerAccounts() {
        return ((Map<UUID, AtomicInteger>)servletContext.getAttribute(ServletContextConst.PLAYER_ACCOUNTS));
    }

    private AtomicInteger getHouseAccount() {
        return ((AtomicInteger)servletContext.getAttribute(ServletContextConst.HOUSE_ACCOUNT));
    }
}
