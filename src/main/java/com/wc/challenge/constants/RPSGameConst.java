package com.wc.challenge.constants;

public class RPSGameConst {
    private RPSGameConst() {}

    public static final Integer GAME_COST = 5;
    public static final Integer GAME_PAYOUT = 10;
}
