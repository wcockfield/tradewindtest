package com.wc.challenge.constants;

public class ServletContextConst {

    private ServletContextConst() {}

    public static final String PLAYER_ACCOUNTS = "playerAccounts";
    public static final String PENDING_GAMES = "pendingGames";
    public static final String HOUSE_ACCOUNT = "houseAccount";

}
