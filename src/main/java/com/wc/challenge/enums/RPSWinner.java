package com.wc.challenge.enums;

public enum RPSWinner {
    PLAYER_ONE,
    PLAYER_TWO,
    DRAW
}
