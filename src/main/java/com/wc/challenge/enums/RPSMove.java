package com.wc.challenge.enums;

public enum RPSMove {
    ROCK,
    PAPER,
    SCISSORS
}
