package com.wc.challenge.enums;

public enum EventType {
    ESTABLISH_NEW_GAME,
    JOIN_GAME
}
