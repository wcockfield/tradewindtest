package com.wc.challenge.error;

public class OperationFailedException extends RuntimeException {

    public OperationFailedException(String msg) {
        super(msg);
    }
}
